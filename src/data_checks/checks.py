"""
Performing rowcounts in Teradata and Snowflake
@author: Matt Milunski
"""

from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
import pandas as pd
import sys

def count_rows(schema_table: tuple, conn: object):
    """Datastore-agnostic row counting function
    Parameters:
        schema_table: A schema-table pair
        conn: A SQLAlchemy connection
    Returns:
        integer 
    """
    try:
        count = pd.read_sql_query(
            f'select count(*) from {schema_table[0]}.{schema_table[1]}', conn)
    except:
        count = pd.read_sql_query(
            f'select cast(count(*) as bigint) from {schema_table[0]}.{schema_table[1]}',
            conn)
    int_out = int(count.values)
    return int_out

def sum_columns(schema_table: tuple, column_name: str ,conn: object):
    """Datastore-agnostic function to sum specific column on a table
    Parameters:
        schema_table: A schema-table pair
        column_name: column to sum
        conn: A SQLAlchemy connection
    Returns:
        float
    """
    try:
        sum_df = pd.read_sql_query(
            f'select sum(nvl({column_name},0)) from {schema_table[0]}.{schema_table[1]}', conn)
    except:
        sum_df = pd.read_sql_query(
            f'select sum(cast(nvl({column_name},0) as float)) from {schema_table[0]}.{schema_table[1]}',
            conn)

    float_out = float(sum_df.values)
    return float_out


def find_numeric_columns(schema_table: tuple, TD_conn: object):
    """Function to find numeric columns on a table in Teradata
    Parameters:
        schema_table: A schema-table pair
        TD_conn: A SQLAlchemy connection for Teradata
    Returns:
        list of strings
    """

    column_df = pd.read_sql_query(f'SELECT ColumnName from dbc.columnsV where DatabaseName = \'{schema_table[0]}\' \
                and tablename = \'{schema_table[1]}\' and ColumnType in (\'I1\', \'I2\', \'I8\', \'I\', \'N\',\'D\', \'F\')', TD_conn)
    column_list = column_df['ColumnName'].to_list()


    return column_list
