"""
Teradata SQLAlchemy Connector - Because the method of specifying it without an 
    abstraction is ugly.
@author: Matt Milunski
"""

from sqlalchemy import create_engine

def create_teradata_engine(ip: str, user: str, password: str):
    return create_engine(f'teradata://{ip}/?username={user}&password={password}')