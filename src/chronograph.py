"""
Chronograph - A module allowing for the writing of event metrics to the database
@author: Matt Milunski
"""

import pandas as pd
from datetime import datetime

def init_table() -> object:
    """Creates a pandas DataFrame with predefined columns for the Snowflake table:
       ABI_POC.MIGRATION_VITALS.TERADATA_SNOWFLAKE_LOAD_TIMES
    params: void
    returns: pandas DataFrame
    """
    return pd.DataFrame(columns=['TABLE_SCHEMA', 'TEST_TYPE', 'COLUMN_NAME', 'TD_RESULT','SF_RESULT','PASS_FAIL','RUN_TSP'])

def time_token():
    """Creates a timestamp that is compliant with Snowflake.
       https://docs.snowflake.com/en/user-guide/python-connector-pandas.html
    params: void
    returns: Timestamp of type pandas.Timestamp(np.datetime64[ns])
    """
    return pd.Timestamp.now()