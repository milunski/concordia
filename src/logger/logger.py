"""
Logging configuration
@author: Matt Milunski
"""

import logging
import os

def check_logging(dirpath, name):
    """Create logging if it doesn't already exist
    Parameters:
        dirpath: Directory path to story log files
        name: File name for logging file
    Returns:
        Void
    """
    log_file = os.path.join(dirpath, name)
    if os.path.exists(log_file) is False:
        os.mkdir(dirpath)
        open(log_file, 'a').close()
    else:
        pass


def logging_config(dirpath, name, level):
    """Build logging configuration
    Parameters:
        dirpath: Directory path to story log files
        name: File name for logging file
    Returns:
        Void
    """
    return logging.basicConfig(
        filename=os.path.join(dirpath, name), 
        filemode='w', level=getattr(logging, level.upper(), None), 
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')