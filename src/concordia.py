from flask import Flask, request, jsonify, abort
import pandas as pd
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
import snowflake.connector as snow
import teradata
import yaml
import socket
import os
from config_loader import configs
import waitress
from request_parser import parser
from data_checks import checks
import tera_engine
import logging
from logger import logger
import chronograph
from datetime import datetime

# Spec app
app = Flask(__name__)

# Load configs
cfg = configs.load_configs('app_config.yaml')
snow_cfg = cfg['snowflake']
tera_cfg = cfg['teradata']
server_cfg = cfg['server']
val_cfg = cfg['validations']
log_cfg = cfg['logs']

# Set up logging
logger.check_logging(dirpath=log_cfg['dirpath'], name=log_cfg['name'])
logger.logging_config(
    dirpath=log_cfg['dirpath'], name=log_cfg['name'], level=log_cfg['level'])

# A greeting!
configs.greeter()

@app.route('/concordia/submit', methods=['POST'])
def execute_submission():
    logging.info(request.url)
    logging.info(request.remote_addr)

    results = chronograph.init_table()

    # Snowflake and Teradata engines
    snow_engine = create_engine(URL(
        account=snow_cfg['account'], 
        user=snow_cfg['user'], 
        password=snow_cfg['password'], 
        database=snow_cfg['database'],
        schema=snow_cfg['schema'], 
        warehouse=snow_cfg['warehouse'],
        role=snow_cfg['role']))

    td_engine = tera_engine.create_teradata_engine(
        ip=tera_cfg['host_ip'],  
        user=tera_cfg['user'],  
        password=tera_cfg['password'])

    result_engine = create_engine(URL(
        account=snow_cfg['account'], 
        user=snow_cfg['user'], 
        password=snow_cfg['password'], 
        database=val_cfg['database'],
        schema=val_cfg['schema'], 
        warehouse=snow_cfg['warehouse'],
        role=snow_cfg['role']))

    # Connections
    snow_conn = snow_engine.connect()
    td_conn = td_engine.connect()
    result_conn = result_engine.connect()

    # Parse request and validate
    schema_table = parser.parse_adf_webhook(
        request=request, regex='[^/]+(?=\\.[\\w]+$)|[^/]+$')
    
    """
    Row count validation
    """
    for schematable in schema_table:
        st_pair = schematable

        # Skip over any invalid tuples
        if len(st_pair) < 2:
            logging.warning(f'{st_pair} is not valid schema-table combination... skipping.')
            continue

        # Compare row counts
        teradata_count = checks.count_rows(
            schema_table=st_pair, conn=td_conn)
        snowflake_count = checks.count_rows(
            schema_table=st_pair, conn=snow_conn)
        if teradata_count == snowflake_count:
            pass_fail = 'PASS'
        else:
            pass_fail = 'FAIL'
        
        # Append results to DF
        results = results.append(
            {'TABLE_SCHEMA': f'{st_pair[0].upper()}.{st_pair[1].upper()}',
             'TEST_TYPE': 'ROW COUNT',
             'COLUMN_NAME': 'N/A',
             'TD_RESULT': teradata_count,
             'SF_RESULT': snowflake_count,
             'PASS_FAIL': pass_fail, 
             'RUN_TSP': chronograph.time_token()
             }, ignore_index = True)
        # logging.info(results)
        teradata_count = checks.count_rows(schema_table=st_pair, conn=td_conn)
        snowflake_count = checks.count_rows(schema_table=st_pair, conn=snow_conn)


        """
        Numeric column summing validation
        """
        numeric_columns = checks.find_numeric_columns(
            schema_table = st_pair, TD_conn = td_conn)
        
        for num_col in numeric_columns:
            teradata_sum = checks.sum_columns(
                st_pair, num_col, td_conn)
            snowflake_sum = checks.sum_columns(
                st_pair, num_col, snow_conn)
            abs_teradata_sum = abs(teradata_sum)
            abs_snowflake_sum = abs(snowflake_sum)
            snowflake_sum_range_start = abs_snowflake_sum * .99999
            snowflake_sum_range_end = abs_snowflake_sum * 1.00001

            if abs_teradata_sum >= snowflake_sum_range_start and abs_teradata_sum <= snowflake_sum_range_end:
                pass_fail = 'PASS'
            else:
                pass_fail = 'FAIL'

            results = results.append(
            {'TABLE_SCHEMA': f'{st_pair[0].upper()}.{st_pair[1].upper()}',
             'TEST_TYPE': 'NUMERIC SUM',
             'COLUMN_NAME': f'{num_col.upper()}',
             'TD_RESULT':teradata_sum,
             'SF_RESULT':snowflake_sum,
             'PASS_FAIL': pass_fail, 
             'RUN_TSP': chronograph.time_token()}, ignore_index = True)
            # logging.info(results)
        results.to_sql(
            name=val_cfg['table'], 
            con=result_engine, 
            if_exists='append', 
            index=False)

    # Close connections and dispose of engines
    td_conn.close()
    snow_conn.close()
    result_conn.close()
    snow_engine.dispose()
    td_engine.dispose()
    result_engine.dispose()
    
    return jsonify('OK'), 200

@app.route('/concordia/test', methods=['GET'])
def test():
    logging.info(request)
    return jsonify("It's me, Concordia!"), 200

if __name__ == '__main__':
    # Retrieve VM's IPv4 and use as server IP
    hostname = socket.gethostname()
    ip_address = socket.gethostbyname(hostname)
    waitress.serve(app, host=ip_address, port=server_cfg['port'])