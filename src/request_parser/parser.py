"""
Parsing the HTTP requests
@author: Matt Milunski
"""

from flask import request
import re

def parse_adf_webhook(request: object, regex: str):
    """Parses the JSON data from an HTTP request
    Parameters:
        request: A Flask `request` object.
        regex: Regex pattern for extracting schema and table names
    Returns:
        Generator of tuples like ((schema1, table1), (schema2, table2), ...)
    """
    # Extract dict from request
    data = request.get_json()

    # Need some API enforcement
    assert len(data.keys()) == 1, 'Only a single key is allowed.'
    assert len(data.values()) == 1, 'Only a single list of values is allowed.'
    key = next(iter(data.keys()))
    assert isinstance(data.get(key), list), 'Value is not a Python list.'

    # Parse each Azure blob path and create a generator of schema-table pairs
    blobpaths = data.get(key)
    schema_tables = (re.search(regex, blob).group() for blob in blobpaths)
    return (tuple(pair.split('#')) for pair in schema_tables)
    