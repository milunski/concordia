"""
Config loader module
@author: Matt Milunski
"""

import yaml
import os

def load_configs(filename):
    cfg_path = os.path.abspath(filename)
    with open(cfg_path) as configs:
        cfg = yaml.load(configs, Loader=yaml.FullLoader)
    return cfg

def greeter():
    print('''

    
    ///////////////////////////////////////
    //                                   //
    //             Concordia             //
    //                                   //
    ///////////////////////////////////////   
                        

    Starting the server...
    
    ''')