# Concordia: A DataOps Tool for Data Validation
Creator: [Matt Milunski](matt.milunski@anheuser-busch.com)  
Contributors:  
- [Mike Deelo](mike.deelo@anheuser-busch.com)


## Description
Concordia is a DataOps application for data validation geared toward Anheuser-Busch's migration from Teradata to Snowflake. The purpose of this software is to perform general QA checks on the data being migrated to ensure consistency, completeness, and accuracy.  

Concordia is built on [Flask](https://flask.palletsprojects.com/en/1.1.x/) and uses Waitress as its WSGI server. It is designed to receive requests following the REST philosophy. When a JSON payload of tables is sent to Concordia, Concordia will start a series of data validations between Teradata and Snowflake. 

## Requirements
- Python 3.6 or higher
- Python's virtual environment package, `virtualenv`. If you don't have it installed, open PowerShell and perform `pip install virtualenv --user`.
- `pip` >= 19.0 
- Developed and tested on Windows 10.
- `Port 5000` is open on the machine running the application and allows inbound traffic. You can set this to your machine by going to _Windows Firewall with Advanced Security_ and adding the port to _Inbound Rules_.  

## Notes
- __Security__: Concordia is designed to run on a virtual machine protected by an internal network using a private IP address. For security purposes, Concordia should not be run on any public network or any computer with a public IP address.

## Directory Structure
TODO

## Operating
### Setting up Concordia 
1. From PowerShell, navigate to the top-level directory (i.e., `concordia`).
2. From PowerShell, create a viritual environment with `python -m venv venv`.
3. Activate the virtual environment with `.\venv\Scripts\activate`. Ensure that you see `(venv)` on the left-hand side of your PowerShell prompt.
4. Install the required packages with `pip3 install -r requirements.txt`.
5. `TEMPLATE_app_config.yaml` needs to be completed with the proper credentials for querying Snowflake and Teradata. Before running Concordia, this YAML file needs to be renamed to `app_config.yaml` for Concordia to find it.

### IP and Port
Concordia adopts the IP address of its host machine in constructing its endpoint and uses the port defined in `app_config.yaml`. 

### Running Concordia
1. Ensure you have the requirements.
2. Open Windows PowerShell.
3. Navigate to the application's top-level directory (i.e., `concordia`).
4. Start the server with `python .\src\concordia.py`.

### Shutting Down Concordia
Shutting down is accomplished with `^C`/`CTRL+C`. This is not a "graceful" shutdown, and any tests that are occurring will be halted and any unsaved results will be lost.

## API
### Calling the API
Concordia expects a `POST` to `http://<concordia ip>:<concordia port>/concordia/submit` with a body of a single JSON object with key `blobkey` and a value of an array for strings. The example below shows an example of how you can invoke the Concordia API with PowerShell:  

```json
Invoke-RestMethod 'http://172.18.200.172:5000/concordia/submit' -Method Post -Body '{"blobkey": ["/path/to/file/<schema>#<table>.txt", "/path/to/file/<schema>#<table>.txt"]}' -ContentType 'application/json'
```

## Troubleshooting
